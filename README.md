# Présentation de Spark

Ce repo contient les ressources nécessaires pour découvrir le framework Spark pour Java.

## Contributeurs 

- DELEVALLEZ Julien
- DELVILLE Lucie
- PENNICK Edouard

## Contenu 

Ce repo contient :
- **Spark.pdf**, le diaporama du cours,
- **TP Spooky Spark.pdf**, le sujet du TP,
- **public.zip**, un dossier de ressources nécessaires pour le TP.
